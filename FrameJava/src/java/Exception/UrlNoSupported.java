/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exception;

/**
 *
 * @author KennyT
 */
public class UrlNoSupported extends Exception {

    private String url;
    
    

    /**
     * @return String return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    public UrlNoSupported(String url) {
        this.url = url;
    }
    @Override
    public String getMessage(){
        return this.url+" : url not found on the storage";
    }

}
