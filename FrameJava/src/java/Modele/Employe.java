/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import Annotation.NewAnnotationType;

/**
 *
 * @author KennyT
 */
public class Employe {
    private String Nom;
    private int id;

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @NewAnnotationType(url="okoko")
    public void Tsysave(){}
}
