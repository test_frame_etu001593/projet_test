/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modele;

import Annotation.NewAnnotationType;
import java.time.LocalDate;
import java.util.HashMap;

/**
 *
 * @author KennyT
 */
public class Person {
    private String Nom;
    private int Age;
    private LocalDate naissance;

    public String getNom() {
        return Nom;
    }

    public void setNom(String Nom) {
        this.Nom = Nom;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int Age) {
        this.Age = Age;
    }

    public LocalDate getNaissance() {
        return naissance;
    }

    public void setNaissance(LocalDate naissance) {
        this.naissance = naissance;
    }

    public Person() {
    }

    public Person(String Nom, int Age, LocalDate naissance) {
        this.Nom = Nom;
        this.Age = Age;
        this.naissance = naissance;
    }
    
    
}
