/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utile;

import java.lang.reflect.Method;

/**
 *
 * @author KennyT
 */
public class ClassMethod {
    private Class<?> className;
    private Method methodName;

    public Class<?> getClassName() {
        return className;
    }

    public void setClassName(Class<?> className) {
        this.className = className;
    }

    public Method getMethodName() {
        return methodName;
    }

    public void setMethodName(Method methodName) {
        this.methodName = methodName;
    }

    public ClassMethod(Class<?> className, Method methodName) {
        this.className = className;
        this.methodName = methodName;
    }
    

}
