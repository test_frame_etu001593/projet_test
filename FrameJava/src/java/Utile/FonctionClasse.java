/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utile;

import Annotation.NewAnnotationType;
import Exception.UrlNoSupported;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 *
 * @author KennyT
 */
public class FonctionClasse {
    public  HashMap<String,ClassMethod> getFillHashmap() {return this.data;}
    private  HashMap<String,ClassMethod> data = new HashMap<String,ClassMethod>();
    
    
    public static Method[] getMethod(Class classe) {
        Method[] meth = classe.getDeclaredMethods();
        return meth;
    }

    public HashMap<String, ClassMethod> MamenoHashmap() throws ClassNotFoundException {

        HashMap<String, ClassMethod> map = new HashMap<String, ClassMethod>();
        Class<?>[] cls = ClassScan.getClasses("Modele");
        for (int l = 0; l < cls.length; l++) {
            Method[] meth = getMethod(cls[l]);
            for (int i = 0; i < meth.length; i++) {
                if (meth[i].getAnnotation(NewAnnotationType.class) != null) {
                    //System.out.println(meth[i].getAnnotation(NewAnnotationType.class).url()+"Classe="+classe.getSimpleName());

                    //System.out.println(meth[i].getName());
                    ClassMethod clm = new ClassMethod(cls[l], meth[i]);
                    map.put(meth[i].getAnnotation(NewAnnotationType.class).url(), clm);
                }
            }
        }
        return map;

    }
     public  boolean find(String urlTofind) throws UrlNoSupported{
        boolean result = false;   
        for(String url: this.getFillHashmap().keySet()){
             if(url.compareTo(urlTofind)==0){
                result = true;
                break;
            }
        }
        if(result == false) throw new UrlNoSupported(urlTofind);   
        return result;
        
    }
    

}
